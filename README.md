# Bootstrapping different applications

## Idea

An app consists mainly of the frontend client, API calls, database and reverse proxy layer.

Can be extended to include auth, mobile clients, etc.
